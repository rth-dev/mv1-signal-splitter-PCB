# mv-1 signal splitter
a little device for analysing nikons MV-1 data reader signals

![idea](https://gitlab.com/rth-dev/mv1-signal-splitter-PCB/-/raw/master/images/the_idea.png)


* **Rev_B** an optimized little helper device
 ![Rev C](https://gitlab.com/rth-dev/mv1-signal-splitter-PCB/-/raw/master/revisions/Rev_B/mv1-signal-splitter%20Rev_B%20front.png)
